package main

import (
	"encoding/json"
	"flag"
	"log"
	"math/rand"
	"net/http"
	"strconv"
)

var httpPort = flag.Int("p", 8080, "HTTP listen port")

func handleGetInt(w http.ResponseWriter, r *http.Request) {
	x := rand.Int()
	data, err := json.Marshal(x)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
		return
	}

	w.Write(data)
}

func handleGetFloat(w http.ResponseWriter, r *http.Request) {
	x := rand.Float64()
	data, err := json.Marshal(x)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
		return
	}

	w.Write(data)
}

func main() {
	flag.Parse()
	http.HandleFunc("/int", handleGetInt)
	http.HandleFunc("/float", handleGetFloat)

	log.Fatal(http.ListenAndServe(":"+strconv.FormatInt(int64(*httpPort), 10), nil))
}
